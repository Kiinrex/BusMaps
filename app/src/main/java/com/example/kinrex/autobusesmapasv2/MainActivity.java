package com.example.kinrex.autobusesmapasv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button Calcular = (Button) findViewById(R.id.todos);
        Calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent res=new Intent(MainActivity.this,MapaTodos.class);
                startActivity(res);

            }
        });

        final Button recorrido = (Button) findViewById(R.id.recorrido);
        recorrido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nom = (EditText) findViewById(R.id.et_Nom);
                String nombre = nom.getText().toString();

                Intent res=new Intent(MainActivity.this,MapRuta.class);
                res.putExtra("nombre",nombre);
                startActivity(res);

            }
        });

    }
}
