package com.example.kinrex.autobusesmapasv2.Objetos;

public class Ubicacion {
    double latitud;
    double longitud;
    long tiempo;

    public Ubicacion() {
    }

    public Ubicacion(double latitud, double longitud, long tiempo) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.tiempo = tiempo;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public long getTiempo() {
        return tiempo;
    }

    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }
}
