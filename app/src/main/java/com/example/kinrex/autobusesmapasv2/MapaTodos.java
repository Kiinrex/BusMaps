package com.example.kinrex.autobusesmapasv2;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.kinrex.autobusesmapasv2.Objetos.FirebaseReferences;
import com.example.kinrex.autobusesmapasv2.Objetos.Ubicacion;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MapaTodos extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    /**
     *  Crea el mapa
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_todos);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Una vez tenemos el mapa, cogeremos los datos del Firebase para poner los markers, solo el
     * el dato con la fecha mas antigua por cada dato
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        final Ubicacion[] ubicacion = {null};

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(FirebaseReferences.ROOT_REFERENCE);

        myRef.addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
                    ubicacion[0] =null;
                    String nombre = postSnapshot.getKey();

                    for(DataSnapshot postpostSnapshot: postSnapshot.getChildren()) {
                        Ubicacion alt = postpostSnapshot.getValue(Ubicacion.class);

                        if(ubicacion[0]==null){
                            ubicacion[0] = new Ubicacion(alt.getLatitud(),alt.getLongitud(),alt.getTiempo());
                        }else if(ubicacion[0].getTiempo()<alt.getTiempo()){
                            ubicacion[0].setLatitud(alt.getLatitud());
                            ubicacion[0].setLongitud(alt.getLongitud());
                            ubicacion[0].setTiempo(alt.getTiempo());
                        }
                    }
                    LatLng ll = new LatLng(ubicacion[0].getLatitud(), ubicacion[0].getLongitud());
                    mMap.addMarker(new MarkerOptions().position(ll).title(nombre));
                }

                LatLng ll = new LatLng(ubicacion[0].getLatitud(), ubicacion[0].getLongitud());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR", databaseError.getMessage());

            }
        });

    }
}
