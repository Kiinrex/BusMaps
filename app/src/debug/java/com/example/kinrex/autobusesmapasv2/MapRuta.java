package com.example.kinrex.autobusesmapasv2;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.kinrex.autobusesmapasv2.Objetos.FirebaseReferences;
import com.example.kinrex.autobusesmapasv2.Objetos.Ubicacion;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MapRuta extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String nombre;


    /**
     *  Crea el mapa y recuperamos la matricula para buscarla
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_ruta);

        nombre = getIntent().getExtras().getString("nombre");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     *  Una vez tenemos el mapa, cogeremos los datos del Firebase para poner los markers y dibujar
     *  la ruta Polyline
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        final Ubicacion[] ubicacion = {null};
        final ArrayList<LatLng> array = new ArrayList<LatLng>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(FirebaseReferences.ROOT_REFERENCE);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
                    ubicacion[0] =null;
                    String nom = postSnapshot.getKey();

                    // Si la matricula coincide con el dato actual entramos
                    if(nombre.equalsIgnoreCase(nom)){
                        for(DataSnapshot postpostSnapshot: postSnapshot.getChildren()) {
                            Ubicacion alt = postpostSnapshot.getValue(Ubicacion.class);

                            LatLng ll = new LatLng(alt.getLatitud(), alt.getLongitud());

                            array.add(ll);

                            mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
                        }
                    }
                }
                // Añadimos el marker del final y el principio
                mMap.addMarker(new MarkerOptions().position(array.get(0)).title(nombre));
                mMap.addMarker(new MarkerOptions().position(array.get(array.size()-1)).title(nombre));

                PolylineOptions poption= new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
                for (int z = 0; z < array.size(); z++) {
                    LatLng point = array.get(z);
                    poption.add(point);
                }
                mMap.addPolyline(poption);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR", databaseError.getMessage());

            }
        });

    }
}
